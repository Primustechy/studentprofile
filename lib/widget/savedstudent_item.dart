import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';
import 'package:student_profile/model/student.dart';
import 'package:student_profile/utils/constants.dart';
import 'package:student_profile/screen/student_info_screen.dart';


class SavedStudentItem extends StatefulWidget {

  Student savedStudent;

  SavedStudentItem(this.savedStudent);

  @override
  _SavedStudentItemState createState() => _SavedStudentItemState();
}

class _SavedStudentItemState extends State<SavedStudentItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          ListTile(
            title: Text(widget.savedStudent.fullName),
            subtitle: Text(widget.savedStudent.phoneNo),
            trailing: IconButton(
              icon: Icon(
                Icons.delete,
                color: Colors.black,
                size: 25.0,
              ),
              onPressed: () {
                buildAlertDialog(
                    'Delete Student',
                    'Are you sure you want to delete ?',
                    'delete', context);
              },
            ),
          ),
          Divider(thickness: 1, color: Colors.black38,),
        ],
      ),
      onTap: () {
        Navigator.of(context).push(
            CupertinoPageRoute(builder: (_)=> StudentInfoPage(widget.savedStudent)));
      },
    );
  }

  buildAlertDialog(String title, String content, String duty, BuildContext context){
    showDialog(
        context: context,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: [
              TextButton(
                child: Text('CANCEL'),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              ),
              TextButton(
                child: Text('OK'),
                onPressed: () {
                  if (duty == 'delete') {
                    FirebaseFirestore.instance
                        .collection(Constants.STUDENTS_COLLECTION)
                        .doc(widget.savedStudent.docId)
                        .delete()
                        .then((_){
                      Toast.show('Student deleted successfully', context,
                          duration: Toast.LENGTH_LONG);
                      Navigator.of(ctx).pop();

                    });
                  }
                },
              ),
            ],
          );
        }
    );
  }
}
