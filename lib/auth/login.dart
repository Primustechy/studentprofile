import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:student_profile/auth/sign_up.dart';
import 'package:student_profile/home_page.dart';
import 'package:student_profile/utils/constants.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email;
  String password;

  // boolean that will help make the password visible
  bool showPassword = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(
            children: [
              SizedBox(height: 40,),
              Align(
                alignment: Alignment.topCenter,
                child: Image.asset('assets/deebuglogo.png'),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Email',
                    suffixIcon: Icon(Icons.email, color: Colors.red,),
                    //prefixIcon: Icon(Icons.email,)
                  ),
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (String val) {
                    email = val;
                  },
                ),
              ),

              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Password',
                    suffixIcon: IconButton(
                      icon: Icon(showPassword ? Icons.visibility : Icons.visibility_off),
                      color: Colors.red,
                      onPressed: () {
                        setState((){
                          showPassword = !showPassword;
                        });
                      },
                    ),
                    //prefixIcon: Icon(Icons.email,)
                  ),
                  obscureText: showPassword ? true : false,
                  onChanged: (String val) {
                    password = val;
                  },
                ),
              ),

              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: MaterialButton(
                  color: Colors.red,
                  child: isUserLogInLoading ? Container(
                    height: 24, width: 24,
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    )
                  ) : Text('LOG IN',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                    )
                  ),
                  onPressed: () {
                    // we will carry out an operation here
                    userLogInCheckTextField();
                  },
                ),
              ),

              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Center(
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Text("Don't have an account ? "),
                      TextButton(
                        child: Text('SIGN UP'),
                        onPressed: () {
                          Navigator.of(context).push(
                              CupertinoPageRoute(builder: (_)=> SignUp()));
                        },
                      )
                    ],
                  ),
                ),
              ),

            ],
          )
        ],
      ),
    );
  }

  bool isUserLogInLoading = false;
  userLogInCheckTextField() {
    isUserLogInLoading = true;
    setState(() {});

    if (email == null || email.isEmpty) {
      isUserLogInLoading = false;
      setState(() {});
      return;
    }

    if (password == null || password.isEmpty) {
      isUserLogInLoading = false;
      setState(() {});
      return;
    }
    // call the function to log user in
    logInUserWithFireBase();
  }

  // Declare the function to sign up user with fire base
  logInUserWithFireBase() {
    setState(() {
      isUserLogInLoading = true;
    });

    // fire base log in user authentication
    FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email, password: password
    ).then((UserCredential credential){
      getUserDataInfoFromFireBase(credential.user.uid);
    }).catchError((e){
      print(e);
      setState(() {
        isUserLogInLoading = true;
      });
    });
  }

  // function declared to save the user info to fire base
  getUserDataInfoFromFireBase(String userId) {
    // we will get the user data info saved to fire base during sign up
   FirebaseFirestore.instance.collection(Constants.USERS_COLLECTION)
       .doc(userId)
       .get().then((userDoc){
         Navigator.of(context).pop();
         Navigator.of(context).push(
             CupertinoPageRoute(builder: (_)=> HomePage()));
   }).catchError((e){
     print(e);
   });
  }

}
