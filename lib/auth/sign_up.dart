import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'package:student_profile/home_page.dart';
import 'package:student_profile/utils/constants.dart';
import 'package:student_profile/auth/login.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {

  // here we will declare variables for the sign up TextFields
  String email;
  String password;
  String confirmPassword;


  bool showPassword = true;
  bool showConfirmPassword = true;
  // boolean that will help make the password and confirmPassword visible
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(
            children: [
              SizedBox(height: 40,),
              Align(
                alignment: Alignment.topCenter,
                child: Image.asset('assets/deebuglogo.png'),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Email',
                    suffixIcon: Icon(Icons.email, color: Colors.red,),
                    //prefixIcon: Icon(Icons.email,)
                  ),
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (String val) {
                    email = val;
                  },
                ),
              ),

              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Password',
                    suffixIcon: IconButton(
                      icon: Icon(showPassword ? Icons.visibility : Icons.visibility_off),
                      color: Colors.red,
                      onPressed: () {
                        setState((){
                          showPassword = !showPassword;
                        });
                      },
                    ),
                    //prefixIcon: Icon(Icons.email,)
                  ),
                  obscureText: showPassword ? true : false,
                  onChanged: (String val) {
                    password = val;
                  },
                ),
              ),

              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Confirm Password',
                    suffixIcon: IconButton(
                      icon: Icon(showConfirmPassword ? Icons.visibility : Icons.visibility_off),
                      color: Colors.red,
                      onPressed: () {
                        setState((){
                          showConfirmPassword = !showConfirmPassword;
                        });
                      },
                    ),
                    //prefixIcon: Icon(Icons.email,)
                  ),
                  obscureText: showConfirmPassword ? true : false,
                  onChanged: (String val) {
                    confirmPassword = val;
                  },
                ),
              ),
              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: MaterialButton(
                  color: Colors.red,
                  child: isUserSignUpLoading ? Container(
                    height: 24, width: 24,
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Colors.white),
                    )
                  ) : Text('SIGN UP',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white
                    ),
                  ),
                  onPressed: () {
                    // we will carry out an operation here
                    userSignUpCheckTextField();
                  },
                ),
              ),

              SizedBox(height: 20,),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Center(
                  child: Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Text("Already have an account ? "),
                      TextButton(
                        child: Text('LOG IN'),
                        onPressed: () {
                          Navigator.of(context).push(
                              CupertinoPageRoute(builder: (_)=> LoginPage()));
                        },
                      )
                    ],
                  ),
                ),
              ),

            ],
          )
        ],
      ),
    );
  }

  // Declare a function to help me check the TextFields before saving to fire base
  bool isUserSignUpLoading = false;
  userSignUpCheckTextField() {
    isUserSignUpLoading = true;
    setState(() {});

    if (email == null || email.isEmpty) {
      isUserSignUpLoading = false;
      setState(() {});
      return;
    }

    if (password == null || password.isEmpty) {
      isUserSignUpLoading = false;
      setState(() {});
      return;
    }

    if (confirmPassword == null || confirmPassword.isEmpty) {
      isUserSignUpLoading = false;
      setState(() {});
      return;
    }

    signUpUserWithFireBase();
  }

  // Declare the function to sign up user with fire base
  signUpUserWithFireBase() {
    setState(() {
      isUserSignUpLoading = true;
    });

    // fire base sign up authentication to implement email & password sign up
    FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email, password: password
    ).then((UserCredential credential){
      saveUserDataToFireBase(credential.user.uid);
    }).catchError((e){
      print(e);
      setState(() {
        isUserSignUpLoading = true;
      });
    });
  }

  // function declared to save the user info to fire base
  saveUserDataToFireBase(String userId) {
    Map<String, dynamic> userDataInfo = Map<String, dynamic>();
    userDataInfo = {
      Constants.USER_ID: userId,
      Constants.USER_EMAIL: email
    };
    // here we will implement fire base to store our user data
    CollectionReference usersRef = FirebaseFirestore.instance.collection(Constants.USERS_COLLECTION);
    usersRef.doc(userId).set(userDataInfo).then((_){
      isUserSignUpLoading = false;
      setState(() {});
      Navigator.of(context).push(
          CupertinoPageRoute(builder: (_)=> HomePage()));
    }).catchError((e){
      print(e);
    });
  }
}