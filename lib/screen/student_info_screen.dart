import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';
import 'package:student_profile/model/student.dart';
import 'package:student_profile/utils/constants.dart';

class StudentInfoPage extends StatefulWidget {

  Student studentInfo;

  StudentInfoPage(this.studentInfo);
  @override
  _StudentInfoPageState createState() => _StudentInfoPageState();
}

class _StudentInfoPageState extends State<StudentInfoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Student Info Page'),
        backgroundColor: Colors.blue[800],
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),

      body: buildStudentInfoPage(),
    );
  }

  buildStudentInfoPage() {
    return Padding(
      padding: EdgeInsets.only(top: 10, right: 8, left: 8),
      child: ListView(
        children: [
          SizedBox(height: 8,),
          Center(
            child: Text('Student Personal Data',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
                color: Colors.purple[900],
                decoration: TextDecoration.underline,
              ),
            ),
          ),

          SizedBox(height: 16,),
          Text(
            'Student Name:',
            style: TextStyle(fontSize: 15),
          ),
          Text(
            widget.studentInfo.fullName,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),

          SizedBox(height: 16,),
          Text(
            'Registered Email:',
            style: TextStyle(fontSize: 15),
          ),
          Text(
            widget.studentInfo.email,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16,
              decoration: TextDecoration.underline,
            ),
          ),

          SizedBox(height: 16,),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                'Contact Number:',
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
              SizedBox(width: 10,),
              Text(
                widget.studentInfo.phoneNo,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ],
          ),

          SizedBox(height: 16,),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                'Institution:',
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
              SizedBox(width: 10,),
              Text(
                widget.studentInfo.school.toUpperCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
            ],
          ),

          SizedBox(height: 16,),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                'Student Location:',
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
              SizedBox(width: 10,),
              Text(
                widget.studentInfo.location.toUpperCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
            ],
          ),

          SizedBox(height: 16,),
          Wrap(
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                'Student Department:',
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
              SizedBox(width: 10,),
              Text(
                widget.studentInfo.department.toUpperCase(),
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
            ],
          ),

          SizedBox(height: 8,),
          Divider(thickness: 2, color: Colors.black38,),

          Align(
            alignment: Alignment.center,
            child: ElevatedButton.icon(
              style: ElevatedButton.styleFrom(primary: Colors.red),
              label: Text(
                'DELETE',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 15
                ),
              ),
              icon: Icon(
                Icons.delete,
                color: Colors.black,
              ),
              onPressed: () {
                // Calling the buildAlertDialog() function to alert us
                // with a confirmation page before we DELETE an active
                // student directly on the app.
                buildAlertDialog(
                    'Delete Student',
                    'Are you sure you want to delete ?',
                    'delete', context);
              },
            ),
          )
        ],
      ),
    );
  }

  buildAlertDialog(String title, String content, String duty, BuildContext context){
    showDialog(
      context: context,
      builder: (BuildContext ctx) {
        return AlertDialog(
          title: Text(title),
          content: Text(content),
          actions: [
            TextButton(
              child: Text('CANCEL'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            ),
            TextButton(
              child: Text('OK'),
              onPressed: () {
                if (duty == 'delete') {
                  FirebaseFirestore.instance
                      .collection(Constants.STUDENTS_COLLECTION)
                      .doc(widget.studentInfo.docId)
                      .delete()
                      .then((_){
                      Toast.show('Student deleted successfully', context,
                          duration: Toast.LENGTH_LONG);
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                  });
                }
              },
            ),
          ],
        );
      }
    );
  }
}
