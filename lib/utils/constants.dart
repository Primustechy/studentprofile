class Constants {
  // Create the Users collection
  static const String USERS_COLLECTION = 'users';

  // Create the users document fields
  static const String USER_ID = 'userId';
  static const String USER_EMAIL = 'email';

  // create the Students Collection
  static const String STUDENTS_COLLECTION = 'students';

  // Create the Students document fields
  static const String DOC_ID = 'docId';
  static const String FULL_NAME = 'fullName';
  static const String EMAIL = 'email';
  static const String PHONE_NO = 'phoneNo';
  static const String TIMESTAMP = 'timeStamp';
  static const String LOCATION = 'location';
  static const String SCHOOL = 'school';
  static const String DEPARTMENT = 'department';

}