import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';

import 'package:student_profile/auth/login.dart';
import 'package:student_profile/home_page.dart';
import 'package:student_profile/utils/constants.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // initializing fire base into a asynchronous
  await Firebase.initializeApp();

  // Using fire base to check if a user is already Logged in
  User user = FirebaseAuth.instance.currentUser;

  if (user == null) {
    runApp(MyApp(false, null));
  } else {
    FirebaseFirestore.instance.collection(Constants.USERS_COLLECTION)
        .doc(user.uid).get().then((DocumentSnapshot docUser){
      runApp(MyApp(true, docUser));
    });
  }

}

class MyApp extends StatelessWidget {
  bool isUserLoggedIn;
  DocumentSnapshot docUser;

  MyApp(this.isUserLoggedIn, this.docUser);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Guess Profile',
      home: !isUserLoggedIn ? HomePage() : LoginPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}


