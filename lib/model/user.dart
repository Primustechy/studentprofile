import 'package:student_profile/utils/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// here we will create a User model for the app
class User {
  String userId;
  String email;

  User({this.userId, this.email});

  factory User.fromSnapshot(DocumentSnapshot doc) {
    return User(
      userId: doc.data()[Constants.USER_ID],
      email: doc.data()[Constants.USER_EMAIL]
    );
  }
}