import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:student_profile/utils/constants.dart';

class Student {
  final String fullName;
  final String docId;
  final String email;
  final String phoneNo;
  final String timeStamp;
  final String location;
  final String school;
  final String department;

  Student({
    this.fullName,
    this.docId,
    this.email,
    this.phoneNo,
    this.timeStamp,
    this.location,
    this.school,
    this.department
});

  factory Student.fromSnapshot(DocumentSnapshot doc) {
    return Student(
      fullName: doc.data()[Constants.FULL_NAME],
      docId: doc.data()[Constants.DOC_ID],
      email: doc.data()[Constants.EMAIL],
      phoneNo: doc.data()[Constants.PHONE_NO],
      timeStamp: doc.data()[Constants.TIMESTAMP].toString(),
      location: doc.data()[Constants.LOCATION],
      school: doc.data()[Constants.SCHOOL],
      department: doc.data()[Constants.DEPARTMENT]
    );
  }

  Map<String,dynamic> toMap() {
    Map<String, dynamic> studentMap = {
      Constants.FULL_NAME: this.fullName,
      Constants.DOC_ID: this.docId,
      Constants.EMAIL: this.email,
      Constants.PHONE_NO: this.phoneNo,
      Constants.TIMESTAMP: this.timeStamp,
      Constants.LOCATION: this.location,
      Constants.SCHOOL: this.school,
      Constants.DEPARTMENT: this.department
    };
    return studentMap;
  }
}