import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';

import 'package:student_profile/utils/constants.dart';
import 'package:student_profile/home_page.dart';

class AddStudent extends StatefulWidget {
  @override
  _AddStudentState createState() => _AddStudentState();
}

class _AddStudentState extends State<AddStudent> {

  // TextEditingControllers declared for text fields to check the inputs
  TextEditingController fullNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController schoolController = TextEditingController();
  TextEditingController departmentController = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Add Students'),
        backgroundColor: Colors.blue[800],
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).push(
                CupertinoPageRoute(builder: (_)=> HomePage()));
          },
        ),
        actions: [
          TextButton(
            child: Text('SAVE', style: TextStyle(fontSize: 16, color: Colors.white)),
            onPressed: formIsUploading ? null : () {
              // function call to save the student info to fire base
              uploadFormToFireBase();
            },
          )
        ],
      ),

      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 10, vertical: 10
        ),
        child: ListView(
          children: [
            SizedBox(height: 10,),
            // Full name TextField
            TextField(
              enabled: !formIsUploading,
              controller: fullNameController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.person, color: Colors.blue,),
                hintText: "Enter Full Name",
                labelText: "Student Name"
              ),
            ),

            SizedBox(height: 20,),
            // Email TextField
            TextField(
              enabled: !formIsUploading,
              controller: emailController,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.email, color: Colors.blue,),
                hintText: "Enter Email",
                labelText: 'Student Email',
              ),
              onTap: () {
                //textFieldIsFilled = false;
                setState(() {});
              },
            ),

            SizedBox(height: 20,),
            // Phone Number TextField
            TextField(
              enabled: !formIsUploading,
              controller: phoneNumberController,
              keyboardType: TextInputType.phone,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.phone, color: Colors.blue,),
                hintText: "Enter Phone Number",
                labelText: 'Phone Number',
              ),
            ),

            SizedBox(height: 20,),
            // Location TextField
            TextField(
              enabled: !formIsUploading,
              controller: locationController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)
                ),
                prefixIcon: Icon(Icons.location_on, color: Colors.blue,),
                hintText: "Enter Location",
                labelText: 'Student Location',
              ),
            ),

            SizedBox(height: 20,),
            // Location TextField
            TextField(
              enabled: !formIsUploading,
              controller: schoolController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.school, color: Colors.blue,),
                hintText: "Enter Institution Name",
                labelText: 'School of Name',
              ),
            ),

            SizedBox(height: 20,),
            // Location TextField
            TextField(
              enabled: !formIsUploading,
              controller: departmentController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                prefixIcon: Icon(Icons.book_outlined, color: Colors.blue,),
                hintText: "Enter Department",
                labelText: 'Student Department',
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool formIsUploading = false;
  uploadFormToFireBase() {
    setState(() {
      formIsUploading = true;
    });

    if (fullNameController.text == null || fullNameController.text == '') {
      print('Please fill a Name');
      setState(() {
        formIsUploading = false;
      });
      return;
    }

    if (emailController.text == null || emailController.text == '') {
      print('Please fill an email');
      setState(() {
        formIsUploading = false;
      });
      return;
    }

    if (phoneNumberController.text == null || phoneNumberController.text == '') {
      print('Please fill a Phone Number');
      setState(() {
        formIsUploading = false;
      });
      return;
    }

    if (locationController.text == null || locationController.text == '') {
      print('Please fill a Name');
      setState(() {
        formIsUploading = false;
      });
      return;
    }

    if (schoolController.text == null || schoolController.text == '') {
      print('Please fill a Name');
      setState(() {
        formIsUploading = false;
      });
      return;
    }

    if (departmentController.text == null || departmentController.text == '') {
      print('Please fill a Name');
      setState(() {
        formIsUploading = false;
      });
      return;
    }

    storeFormDataToFireStore();
    Toast.show('Student saved successfully', context, duration: Toast.LENGTH_LONG);
  }

  storeFormDataToFireStore() {
    int timeStamp = DateTime.now().millisecondsSinceEpoch;

    FirebaseFirestore.instance.collection(Constants.STUDENTS_COLLECTION)
        .doc(timeStamp.toString()).set({
      Constants.FULL_NAME: fullNameController.text,
      Constants.EMAIL: emailController.text,
      Constants.PHONE_NO: phoneNumberController.text,
      Constants.LOCATION: locationController.text,
      Constants.SCHOOL: schoolController.text,
      Constants.DEPARTMENT: departmentController.text,
      Constants.TIMESTAMP: timeStamp,
      Constants.DOC_ID: timeStamp.toString(),
    }).then((_){
      clearInputTextFields();
    }).catchError((e){
      print(e);
      setState(() {
        formIsUploading = false;
      });
    });
  }

  clearInputTextFields() {
    fullNameController.text = '';
    emailController.text = '';
    phoneNumberController.text = '';
    locationController.text = '';
    schoolController.text = '';
    departmentController.text = '';
    formIsUploading = false;

    setState(() {});
  }
}