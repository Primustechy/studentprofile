import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:student_profile/form/student_form.dart';
import 'package:student_profile/model/student.dart';
import 'package:student_profile/utils/constants.dart';
import 'package:student_profile/widget/savedstudent_item.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Students'),
        backgroundColor: Colors.deepOrange,
        elevation: 0.0,
      ),

      body: buildStudentStreamList(),

      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Colors.deepOrangeAccent,
        label: Text("Add",
          style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold
          ),
        ),
        icon: Icon(
          Icons.person_add_alt_1_outlined,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.of(context).push(
              CupertinoPageRoute(builder: (_)=> AddStudent()));
        },
      ),
    );
  }

  buildStudentStreamList() {
    return StreamBuilder(
      stream: FirebaseFirestore.instance.collection(Constants.STUDENTS_COLLECTION)
          .orderBy(Constants.TIMESTAMP, descending: true).snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snap) {
        if (!snap.hasData) {
          return Container(
            height: 100, width: 100,
            child: CircularProgressIndicator(),
          );
        }
        return ListView.builder(
          itemCount: snap.data.docs.length,
          itemBuilder: (BuildContext context, int index) {
            DocumentSnapshot studentSnapshotItem = snap.data.docs[index];
            Student studentItem = Student.fromSnapshot(studentSnapshotItem);
            return SavedStudentItem(studentItem);
          },
        );
      },
    );
  }
}

